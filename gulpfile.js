var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css');

// js:
gulp.task('minify-js', ['minify-root-js'], function() {
  return gulp.src(['skin/frontend/**/*.js', '!**/*/onestepcheckout/**/*.js', '!**/*/sagepaysuite/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('skin/frontend'))
});

gulp.task('minify-root-js', ['minify-css'], function() {
  return gulp.src(['js/**/*.js', '!js/prototype/**/*.js', '!**/*/adminhtml/**/*.js', '!**/*/sagepaysuite/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('js'))
});

// css:
gulp.task('minify-css', ['minify-css-media'], function() {
  return gulp.src('skin/frontend/**/*.css')
    .pipe(minify({
    compatibility: 'ie7',
    processImport: false
    }))
    .pipe(gulp.dest('skin/frontend'));
});

// in case of any files merged by Magento:
gulp.task('minify-css-media', function() {
  return gulp.src('media/**/*.css')
    .pipe(minify({compatibility: 'ie7',processImport: false}))
    .pipe(gulp.dest('media'));
});

gulp.task('default', function() {
    gulp.start('minify-js');
});
